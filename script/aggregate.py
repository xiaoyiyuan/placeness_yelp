import csv
import numpy as np
import os
import json
from collections import defaultdict


vocab_index = np.load("../data/prep_data/vocab.index.npy").item()
index_vocab = np.load("../data/prep_data/index.vocab.npy").item()
emb = np.load("../data/prep_data/emb.npy")

'''
    Step1: gather all aspects_.csv file into one so it's consistent with reviews.json
            Individual aspects_.csv files are based yelp_.npy
            But! The aspects_.csv is not 10,000 per file because empty rows in yelp_.npy
            So, put the empty rows back to aspects and merge all to one csv file and export it.
                339 * 10000 + 9993 = 3,399,993
'''

def exportAllAspect(path, output_file):
    '''
        It finds all the missing rows from yelp_.npy data and add altogether to one file.
    '''

    count = 0
    for number in range(0, 340):
        for filename in os.listdir(path):
            file_number = filename.__repr__()[:-5][9:]
            if str(number) == file_number:
                print("now processing:", file_number)
                yelp_file = np.load("../data/prep_data/predict_yelp/"+"yelp_"+file_number+".npz")['data']
                empty_rows = []
                i = 0
                for row in yelp_file:
                    if row.size == 0:
                        empty_rows.append(i)
                    i += 1
                aspect_file = open(path + filename, 'r')
                aspects = list(csv.reader(aspect_file))
                for row in empty_rows:
                    aspects.insert(row, [])
                count += len(aspects)
                print("--number of rows:", len(aspects))
                print("---number of total rows", count)
                assert len(aspects) == 10000 or file_number == '339'
                with open(output_file,'a') as output:
                    writer = csv.writer(output)
                    writer.writerows(aspects)

'''
    step 2: aggregate aspects to higher level concepts (categories)
            This can be very tricky. If this step is messed up all the following analysis is useless
'''

category1 = {"chef": ["chef"],
              "ingredient": ["ingredient", "ingredients"],
              "quality": ["quality"],
              "portion": ["portion", "portions"],
              "texture": ["texture", "textures"],
              "taste": ["taste", "flavor", "flavors", "tastes"],
              "atmosphere":["atmosphere", "vibe", "ambiance", "vibes"],
              "experience":["experience", "experiences"],
              "reservation": ["reservation", "reservations"],
              "crowded": ["crowded", "wait", "waiting"],
              "menu":["menu", "options", "option", "choices"],
              "location": ["location", "locations"]}

category2 = {"food": ["food Food foods", 'food dish meal', 'entree', "seafood",
                      "vegetable vegetables", "meat beef steak chicken poultry",
                      "pasta", "pizza", "soup", "cheese", "sauce", "rice beans",
                      "appetizers appetizer", "burger sandwich sandwiches", "salad salads"],
             "dessert": ["dessert desserts sweet sugar"],
             "drink": ["drink wine tea alcohol soda"],
             "place": ['place restaurant'],
             "service": ["service Service server waiter waitress crew people owner"],
             "interior": ["seating room table layout area"],
             "price": ["price pricing prices priced cost costs deals bill bills bucks"]}
#todo: maybe try re-fine price later (move some wrong ones to "", do not redo the categories may change others)

def getDictValues(dict):
    category = []
    for lst in dict.values():
        for item in lst:
            category.append(item)
    return category

def getEmbeddings(token):
    if len(token.split(" ")) == 1:
        return emb[vocab_index[token]]
    else:
        embeddings = np.zeros((300,))
        phrase = token.split(" ")
        for word in phrase:
            embeddings += emb[vocab_index[word]]
        return embeddings

def getKey(aspect, category1):
    '''
        For aspects in category1 values
        find their key and assign the key as their category
    '''
    # get words in the aspect is in category1 values (only care about the first one if many)
    word = [i for i in aspect.split(" ") if i in getDictValues(category1)][0]
    key = [key for key, value in category1.items() if word in value]
    return key[0]

def getClosestKey(aspect, category2):
    '''
        For aspects not in category1 values, get the closest category2 values
        find their key and assign the key as their category
        Important note: the theoretical consine similarity is between -1 to 1
                        if the max_similarity is below 0,
                            I assume it cannot find a good category to represent the aspect
                            so return ""
    '''
    vec1 = getEmbeddings(aspect)
    if vec1.sum() == 0: # a lot of vocab cannot find its embeddings...
        return ""
    category2_emb = [getEmbeddings(i) for i in getDictValues(category2)]
    category2_values = getDictValues(category2)
    max_similarity = 0
    closest_value = None
    for index, embedding in enumerate(category2_emb):
        vec2 = embedding
        similarity = np.sum(vec1*vec2)/(np.linalg.norm(vec1)*np.linalg.norm(vec2))
        if similarity > max_similarity:
            max_similarity = similarity
            closest_value = category2_values[index]
    closest_key = [key for key, value in category2.items() if closest_value in value]
    if len(closest_key) > 0:
        return closest_key[0]
    else:
        return ""

def categorizeAList(aspect_list, category1, category2):
    '''
        Turn a list of aspects into a list of categories
        For phrases, add all word's embeddings together
    '''
    output = []
    for aspect in aspect_list:
        if set(aspect.split(" ")).intersection(getDictValues(category1)) != set():
            key = getKey(aspect, category1)
            output.append(key)
        else:
            key = getClosestKey(aspect, category2)
            output.append(key)
    return output

def categorizeAll(aspect_all_file, categories_all_file):
    '''
        Read in aspect_all.csv, Export categories_all.csv
    '''
    aspall = list(csv.reader(open(aspect_all_file)))
    catall = []
    n = 0
    for aspect_list in aspall:
        if n % 10000 == 0:
            print("Now categorizing the aspect in row: ", n, "out of ", len(aspall))
        category_list = categorizeAList(aspect_list, category1, category2)
        catall.append(category_list)
        n += 1
    with open(categories_all_file,"w") as outputfile:
        writer = csv.writer(outputfile)
        writer.writerows(catall)

def categorizeSanityCheck(aspect_all_file, categories_all_file):
    '''
        check aspects_all and  categories_all one to one
        ONLY use this function when the length of aspect_all = categories_all
    '''
    catall = list(csv.reader(open(categories_all_file)))
    aspall = list(csv.reader(open(aspect_all_file)))
    temp = defaultdict(set)
    for num in range(len(catall)):
        for delvt, pin in zip(catall[num], aspall[num]):
            temp[delvt].add(pin)
    return temp
# "price" category is a bit bad, so exclude it in analysis first

'''
    Step 3: add aspects and categories to review data and export
'''

def addToJson(res_review, categories_all, aspect_all):
    print("Now write add aspects and categories to the res_review.json file")
    res_file = open(res_review)
    for file in res_file:
        jsons = json.loads(file)
    cat_file = open(categories_all, 'r')
    asp_file = open(aspect_all, 'r')
    categories = list(csv.reader(cat_file))
    aspects = list(csv.reader(asp_file))
    for i in range(len(jsons)):
        jsons[i]['aspect_categories'] = categories[i]
        jsons[i]['aspects'] = aspects[i]
        if i % 100000 == 0:
            print("now processing: ", i)
    print("Now dumping the res_review_cat.json file")
    with open('../data/result_data/res_review_cat.json', 'w') as f:
        json.dump(jsons, f, ensure_ascii=False)


if __name__ == '__main__':
    path = "../data/result_data/aspects/"
    res_review = "../data/prep_data_yelp/res_review.json"
    categories_all = "../data/result_data/categories_all.csv"
    categories_test = "../data/result_data/categories_test.csv"
    aspects_all = "../data/result_data/aspects_all.csv"

    # exportAllAspect(path, aspects_all)
    categorizeAll(aspects_all, categories_all)
    addToJson(res_review, categories_all, aspects_all)
    dict = categorizeSanityCheck(aspects_all, categories_all)