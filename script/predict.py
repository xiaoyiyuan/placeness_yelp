import tensorflow as tf
import os
import numpy as np
from itertools import groupby
from operator import sub, itemgetter
import csv
from model import Model
import utils

class PredictModel(Model):

    def initializeData(self):
        with tf.name_scope("data"):
            self.x = tf.placeholder(shape=[None, 85],dtype=tf.int32)
            self.y = tf.placeholder(shape=[None, 85],dtype=tf.int32)
            self.lens = tf.placeholder(shape=[None,],dtype=tf.int32)
            self.embeddings = utils.getEmbeddings()



    def predict(self, path, index_vocab):
        with tf.Session() as sess:
            sess.run(tf.global_variables_initializer())

            saver = tf.train.Saver()
            ckpt = tf.train.get_checkpoint_state(
                os.path.dirname('../checkpoints/original_model_yelp/checkpoint'))
            if ckpt and ckpt.model_checkpoint_path:
                saver.restore(sess, ckpt.model_checkpoint_path)
                # saver.restore(sess, 'checkpoints/original_model_yelp/model-1258')

            step = model.global_step.eval()
            print("~~~~~The prediction is from model of step", step,'~~~~~')

            i = 0
            for filename in os.listdir(path):
                if not filename.startswith('.'):
                    i += 1
                    file_number = filename.__repr__()[:-5][6:]
                    print("Predicting file " + file_number +" " + str(i)+"/298")
                    reviews = np.load(path + filename)['data']
                    aspects = []
                    for review in reviews:
                        # I cut all sentences > 85 in parsePredictData(), so some reviews are empty
                        if review.size != 0:
                            review = review.astype(np.int32)
                            lens = utils.getXLen(review)
                            prediction = sess.run(self.predictions, feed_dict={self.x: review,
                                                                               self.lens: lens})
                            review_aspects = getAspects(review, lens, prediction, index_vocab)
                            aspects.append(review_aspects)
                # Do not write all aspects to a giant csv file
                # because when iterating the directory, it's processing order is not what you want
                # you want to retain the order so it can find its position at res.review.json
                    with open("../data/result_data/aspects/aspects_" + file_number + ".csv", 'w') as file:
                        print("output file " + file_number)
                        writer = csv.writer(file)
                        writer.writerows(aspects)

def transform(x, x_lens, predictions, index_vocab):
    '''
        transform sentences represented by indices and their labels
        into sentences of real words and their aspect targets of real words
    '''
    sentences = []
    for s in x:
        sentence = []
        for i in s:
            if i != 0:
                word = index_vocab[i]
                sentence.append(word)
        sentences.append(sentence)
    tags = []
    x_lens = x_lens.astype(int)
    start = 0
    for len in x_lens:
        tag = predictions[start:start + len]
        tags.append(tag)
        start += len
    return sentences, tags

def getPhrases(aspect_indices):
    iget = itemgetter(1)
    gb = groupby(enumerate(aspect_indices), key=lambda x:sub(*x))
    all_groups = (list(map(iget, g)) for _,g in gb)
    phrases = list(filter(lambda x:len(x)>1, all_groups))
    return phrases

def getAspects(x, x_lens, predictions, index_vocab):
    sentences, tags = transform(x, x_lens, predictions, index_vocab)
    aspects = []
    for i, s in enumerate(sentences):
        tag = tags[i]
        aspect_indices = [index for index, value in enumerate(tag) if value != 0]
        if aspect_indices != []:
            phrases = getPhrases(aspect_indices)
            for phrase in phrases:
                words = ' '.join([s[i] for i in phrase])
                aspects.append(words)
            phrases_flat = [val for sublist in phrases for val in sublist]
            for word in aspect_indices:
                if word not in phrases_flat:
                    aspects.append(s[word])
    return aspects

if __name__ == '__main__':
    model = PredictModel()
    model.build()
    model.predict(path = "../data/prep_data/predict_yelp/",
                  index_vocab = np.load("../data/prep_data/index.vocab.npy").item())
