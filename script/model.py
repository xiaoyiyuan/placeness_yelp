import tensorflow as tf
import time
import os
import utils
from base_model import BaseModel

class Model(BaseModel):
    def __init__(self):
        self.batch_size = 32
        self.learning_rate = 0.001
        self.nclass = 3
        self.global_step = tf.Variable(0, dtype=tf.int32, trainable=False, name='global_step')
        self.eval_step = 10

    def initializeData(self):
        with tf.name_scope("data"):
            # train, test = utils.getSemEvalData()
            # train, test = utils.getMixedData()
            train, test, test_size = utils.getYelpData()
            train = train.shuffle(10000)
            train, test = train.batch(self.batch_size), test.batch(test_size)
            iterator = tf.data.Iterator.from_structure(train.output_types, train.output_shapes)
            self.train_init = iterator.make_initializer(train)
            self.test_init = iterator.make_initializer(test)
            self.lens, x, y = iterator.get_next()
            self.x = tf.cast(x, tf.int32)
            self.y = utils.pack_y(self.lens, y)
            self.embeddings = utils.getEmbeddings()

    def buildLayers(self):
        lookup = self.emb_lookup(batch_data=self.x,
                                 emb=self.embeddings,
                                 scope_name="lookup")
        self.prob = tf.placeholder_with_default(1.0, [], 'keep_prob')
        dropout0 = tf.nn.dropout(lookup, keep_prob=self.prob, name='dropout0')
        conv1 = self.convRelu(dropout0, 128, 5, 1, 'SAME', 'conv1')
        conv2 = self.convRelu(dropout0, 128, 3, 1, 'SAME', 'conv2')
        conv12 = tf.concat([conv1, conv2], axis=2,name='conv12')
        dropout1 = tf.nn.dropout(conv12, keep_prob=self.prob, name='dropout1')
        conv3 = self.convRelu(dropout1, 256, 5, 1, 'SAME', 'conv3')
        dropout2 = tf.nn.dropout(conv3, keep_prob=self.prob, name='dropout2')
        conv4 = self.convRelu(dropout2, 256, 5, 1, 'SAME', 'conv4')
        dropout3 = tf.nn.dropout(conv4, keep_prob=self.prob, name='dropout3')
        conv5 = self.convRelu(dropout3, 256, 5, 1, 'SAME', "conv5")
        conv5 = tf.reshape(conv5, [-1, 256]) # from (batch, 85, 256) to (batch*85, 256)
        logits = self.fully_connected(conv5, self.nclass, "fc")
        self.logits = utils.pack_logits(self.lens, logits)

    def getLoss(self):
        with tf.name_scope("loss"):
            cross_entropy = tf.nn.softmax_cross_entropy_with_logits_v2(labels=self.y,
                                                                       logits=self.logits,
                                                                       name="cross_entropy")
            self.loss = tf.reduce_mean(cross_entropy, name="loss")

    def optimize(self):
        self.opt = tf.train.AdamOptimizer(self.learning_rate).minimize(self.loss,
                                                                       global_step=self.global_step)

    def getScores(self):
        with tf.name_scope('predict'):
            predictions = tf.nn.softmax(self.logits)
            self.predictions = tf.argmax(predictions, 1)
            self.label = tf.argmax(self.y, 1)
            cmatrix = tf.confusion_matrix(self.label, self.predictions, num_classes=3)
            # TP, FP, FN only counting label 1 or 2 (the label is very unbalanced if count 0).
            TP = cmatrix[1, 1] + cmatrix[2, 2]
            FP = cmatrix[0, 1] + cmatrix[2, 1] + cmatrix[0, 2] + cmatrix[1, 2]
            FN = cmatrix[1, 0] + cmatrix[1, 2] + cmatrix[2, 0] + cmatrix[2, 1]
            self.precision = TP / (TP + FP)
            self.recall = TP / (TP + FN)
            self.f1 = (2 * self.precision * self.recall) / (self.precision + self.recall)

    def getSummaries(self):
         with tf.name_scope("summaries"):
            tf.summary.scalar("loss", self.loss)
            tf.summary.scalar("f1", self.f1)
            tf.summary.scalar("precision", self.precision)
            tf.summary.scalar("recall", self.recall)
            tf.summary.histogram("train_histogram_loss", self.loss)
            self.summary_op = tf.summary.merge_all()


    def build(self):
        self.initializeData()
        self.buildLayers()
        self.getLoss()
        self.optimize()
        self.getScores()
        self.getSummaries()


    def train_one_epoch(self, sess, saver, init, writer, epoch, step):
        start_time = time.time()
        sess.run(init)
        total_loss = 0
        total_f1 = 0
        n_batches = 0
        try:
            while True:
                _, l, f1_batch, summaries = sess.run([self.opt,
                                                      self.loss,
                                                      self.f1,
                                                      self.summary_op],
                                                     feed_dict={self.prob: 0.5})
                total_loss += l
                total_f1 += f1_batch
                step += 1
                n_batches += 1
                writer.add_summary(summaries, self.global_step.eval())
                if (step + 1) % self.eval_step == 0:
                    print('Loss at batch {0}: {1}'.format(step, l))
        except tf.errors.OutOfRangeError:
            pass

        saver.save(sess, '../checkpoints/original_model_yelp/model', step)

        print("================= epoch ", epoch, " summary ====================")
        print('Average train loss:', total_loss / n_batches)
        print('Average train F1:', total_f1 / n_batches)
        print('Took: {0} seconds'.format(round(time.time() - start_time), 2))
        return step

    def evaluate(self, sess, init, writer, epoch, step):
        start_time = time.time()
        sess.run(init)  # initiate test data here
        total_loss,total_f1, total_prec, total_rec = 0, 0, 0,0
        n_batch = 0
        try:
            while True:
                # the dropout layer is 1.0 by default so no need to feed_dict here
                f1_batch, prec, rec, l, summaries = sess.run([self.f1,
                                                              self.precision,
                                                              self.recall,
                                                              self.loss,
                                                              self.summary_op])
                total_loss += l
                total_f1 += f1_batch
                total_prec += prec
                total_rec += rec
                n_batch += 1
                writer.add_summary(summaries, global_step=step)
        except tf.errors.OutOfRangeError:
            pass
        print("Average test loss:", total_loss/n_batch)
        print('Average test F1:', total_f1 / n_batch)
        print('Average precision recall:', total_prec / n_batch, total_rec / n_batch)
        print('Took: {0} seconds'.format(round(time.time() - start_time), 2))
        print("========================================================")


    def train(self, n_epochs):
        try:
            os.mkdir('../checkpoints')
            os.mkdir('../checkpoints/original_model_yelp')
        except OSError:
            pass

        train_writer = tf.summary.FileWriter('../graphs/original_model_yelp/train',
                                             tf.get_default_graph())
        test_writer = tf.summary.FileWriter('../graphs/original_model_yelp/test',
                                            tf.get_default_graph())

        with tf.Session() as sess:
            sess.run(tf.global_variables_initializer())

            saver = tf.train.Saver(max_to_keep=None)
            ckpt = tf.train.get_checkpoint_state(
                os.path.dirname('../checkpoints/original_model_yelp/checkpoint'))
            if ckpt and ckpt.model_checkpoint_path:
                saver.restore(sess, ckpt.model_checkpoint_path)

            step = self.global_step.eval()

            for epoch in range(n_epochs):
                step = self.train_one_epoch(sess, saver, self.train_init,
                                            train_writer, epoch, step)
                self.evaluate(sess, self.test_init, test_writer, epoch, step)
        train_writer.close()
        test_writer.close()


if __name__ == '__main__':
    model = Model()
    model.build()
    model.train(n_epochs=20)
