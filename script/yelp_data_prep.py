import numpy as np
import re
import json
import csv
from nltk.tokenize import sent_tokenize
import random
from random import shuffle
from data_prep import Indexer
import data_prep

'''
    This script contains 3 steps for processing Yelp data:
        Stage 1: select reviews from Yelp for labeling
        Stage 2: prep the labeled data to train the model
        Stage 3: prep all the rest of Yelp data for prediction
            (Note that the data not only contains sentences but also other info for further analysis)
'''

'''
    Stage 1: select reviews for labeling and model training
        1. get restaurant business.json (export it in res_business.json)
        2. get restaurant business_id (export it in csv)
        3. using business_id and get their reviews, shuffle, and export it in csv
        4. label the reviews in csv
    Stupid thing I've done:
        I selected got ALL of restaurant reviews, then select randomly those for training
        (which is very slow, oh well)
'''

def exportResBusiness():
    '''
        select restuarants in 8 states, export it as business.json
        total number of businesses: 37,818
    '''
    res_business = []
    input = "../data/official_data/yelp_dataset/business.json"
    states = ['AZ', 'NC', 'NV', 'OH', 'PA', 'WI', 'IL', 'SC']
    yes_categories = ['Restaurants', 'Restaurant', 'Food']
    no_categories = ['Butcher', 'CSA', 'Convenience Stores', 'Custom Cakes', 'Farmers Market',
                     'Food Delivery Services', 'Food Trucks', 'Grocery', 'Honey',
                     'International Grocery','Kombucha', 'Meaderies','Specialty Food',
                     'Street Vendors', 'Wineries', 'Water Stores']
    files = open(input)
    for f in files:
        j = json.loads(f)
        if j['categories'] != None:
            categories = list(j['categories'].split(', '))
            if len([item for item in yes_categories if item in categories]) != 0:
                if len([item for item in no_categories if item in categories]) == 0:
                    if j['state'] in states:
                        res_business.append(j)
    print(len(res_business))
    with open('../data/prep_data_yelp/res_business.json', 'w') as f:
        json.dump(res_business, f, ensure_ascii=False)

def exportResBid():
    '''
        get business_id from res_business.json, export it as res_bid.csv
    '''
    input = "../data/prep_data_yelp/res_business.json"
    resid = []
    with open(input, 'r') as f:
        jsons = json.load(f)
    for j in jsons:
        id = j['business_id']
        resid.append(id)
    with open("../data/prep_data_yelp/res_bid.csv","w") as outfile:
        writer = csv.writer(outfile)
        writer.writerow(resid)

def exportReviews():
    '''
        select from review.json using business_id
        export {business_id, review_id, user_id, sentences} as in res_review.json
    '''
    res_bid = "../data/prep_data_yelp/res_bid.csv"
    review = "../data/official_data/yelp_dataset/review.json"
    resid = None
    with open(res_bid, 'r') as inputfile:
        reader = csv.reader(inputfile)
        for id in reader:
            resid = id
    with open(review) as file:
        i = 0
        res_review = []
        for f in file:
            j = json.loads(f)
            if j['business_id'] in resid:
                row = {'business_id':j['business_id'],
                       'review_id':j['review_id'],
                       'user_id':j['user_id'],
                       'review':j['text']}
                res_review.append(row)
                i += 1
                if i % 100 == 0:
                    print(row)
    with open('../data/prep_data_yelp/res_review.json', 'w') as f:
        json.dump(res_review, f, ensure_ascii=False)


def exportSelections():
    '''
        select sentences for hand labeling
        (the res_review.csv file is replaced by res_review.json, so the function does not work)
    '''
    input = "../data/prep_data_yelp/res_review.csv"
    # select 3000 random sentences (length >1)
    random_num=[]
    for x in range(5000):
        num = random.randint(0,14070277)
        random_num.append(num)
    select = []
    i = 0
    with open(input,'r') as file:
        for row in file:
            i += 1
            if i in random_num and len(row) > 1:
                select.append(row)
    if len(select) > 3000:
        select = select[:3000]
    for i in range(len(select)):
       select[i] = select[i][:-1] #get rid of "\n" in the end
    with open("../data/prep_data_yelp/res_review_selection.csv",'w') as output:
        writer = csv.writer(output, delimiter='\n')
        writer.writerow(select)

# exportResBusiness()
# exportResBid()
# exportReviews()
# exportSelections()

'''
    Stage 2: transform sentences and targets to train/test X, y for the model: 
        sentences.csv, targets.csv (optional)
        train_yelp.npz, test_yelp.npz
        updated vocab.index.npy, index.vocab.npy, emb.npy
'''
def parseLabeledData(yelpfile, save=False):
    sentences=[]
    targets=[]
    i = 0
    with open(yelpfile, newline='') as csvfile:
        for row in csvfile:
            if i%2 == 0:
                sentences.append(row[:-1])
            if i%2 == 1:
                targets.append(row[:-1])
            i += 1
    targets.append("")
    # save them for the record
    if save:
        with open("../data/prep_data_yelp/sentences.csv",'w') as output:
            writer=csv.writer(output, delimiter=',',quoting=csv.QUOTE_ALL)
            for i in range(len(sentences)):
                writer.writerow([sentences[i]])
        with open("../data/prep_data_yelp/targets.csv",'w') as output:
            writer = csv.writer(output, delimiter=',')
            for i in range(len(targets)):
                targets[i] = targets[i].split(', ')
                writer.writerow(targets[i])
    # cleanining and formatting into list of sentence arrays
    for i in range(len(targets)):
        targets[i] = targets[i].split(', ')
        targets[i] = [words.split(' ') for words in targets[i]] #put each phrase in its own []
    sentences_=[]
    iobs= []
    for sid, sentence in enumerate(sentences):
        r = re.compile(r'([.,/#!$%^&*;:{}=_`~()-])[.,/#!$%^&*;:{}=_`~()-]+')
        sentence = r.sub(r'\1', sentence)
        words = re.findall(r"[\w']+|[.,!?;]", sentence)
        iob = [0] * len(words)
        for lst in targets[sid]: #target[7]:[['Beef', 'Carpaccio'], ['Salad']]
            if lst != ['']:
                index1 = words.index(lst[0])
                iob[index1] = 1
                if len(lst) > 1:
                    for index2 in range(index1+1, index1+len(lst)):
                        iob[index2] = 2
        iobs.append(np.array(iob))
        sentences_.append(np.array(words))
    return sentences_, iobs

def partition(sentences_, iobs, rate):
    indices = [index for index, element in enumerate(sentences_)]
    shuffle(indices)
    train_indices = indices[:int(rate*len(sentences_))]
    test_indices = indices[int(rate*len(sentences_)):]
    train_sentences, train_iobs, test_sentences, test_iobs = [], [], [], []
    for index in train_indices:
        train_sentences.append(sentences_[index])
        train_iobs.append(iobs[index])
    for index in test_indices:
        test_sentences.append(sentences_[index])
        test_iobs.append(iobs[index])
    return train_sentences, train_iobs, test_sentences, test_iobs


def processYelpTrain(yelp_labeled_file, embedding_file, vocab_index_file, index_vocab_file, path):
    '''
        Add to original vocab_index, index_vocab, and redo emb.npy based on new expanded vocab_index
    '''
    print("...Now parsing the labeled data...")
    sentences, iobs = parseLabeledData(yelp_labeled_file, save=False)
    train_sentences, train_iobs, test_sentences, test_iobs = partition(sentences, iobs, 0.8)
    sentence_length = 85

    print("...Now indexing sentences and get embeddings...")
    indexer = Indexer([train_sentences, test_sentences])
    vocab_index, index_vocab = indexer.indexVocab(vocab_index_file, index_vocab_file)
    sorted_embeddings = data_prep.getSortedEmbeddings(vocab_index, embedding_file, 300)
    train_X = indexer.indexSentence(train_sentences, vocab_index, sentence_length)
    train_y = data_prep.adjust_iobs_length(train_iobs, sentence_length)
    test_X = indexer.indexSentence(test_sentences, vocab_index, sentence_length)
    test_y = data_prep.adjust_iobs_length(test_iobs, sentence_length)

    print("...Now writing to files...")
    np.save(path+"vocab.index.npy", vocab_index)
    np.save(path+"index.vocab.npy", index_vocab)
    np.save(path+"emb.npy", sorted_embeddings)
    np.savez(path+"train_yelp.npz", X=train_X, y=train_y)
    np.savez(path+"test_yelp.npz", X=test_X, y=test_y)

'''
    Stage 3: prep all reviews needed for analysis
        1. filter reviews (i.e. US only, English only, res only etc)
        2. The sentences must have information about business_id, user_id, and review_id
'''

def parsePredictData(jsons, start, end):
    '''
        res_review has 3,399,993 reviews
        each review is a list of sentences arrays
        so overall it returns a big list that has lists of sentence arrays (like a 3d array).
    '''
    i = 0
    output = []
    for j in jsons[start:end]:
        j['id'] = start + i
        review = j['review']
        sentences = sent_tokenize(review)
        sentences_ = []
        for sentences in sentences:
            r = re.compile(r'([.,/#!$%^&*;:{}=_`~()-])[.,/#!$%^&*;:{}=_`~()-]+')
            sentences = r.sub(r'\1', sentences)
            words = re.findall(r"[\w']+|[.,!?;]", sentences)
            if len(words) <= 85:
                sentences_.append(np.array(words))
        i += 1
        output.append(sentences_)
    return output

def getIntervals(length):
    lst = []
    for i in range(length):
        if i % 10000 == 0:
            lst.append(i)
    lst.append(length)
    return lst

def processYelpPredict(res_review, embedding_file, vocab_index_file, index_vocab_file, path):
    # The file is big so cut into every 10,000 reviews.
    # Each review is like a train/test, 2d array: [(sentence1)
    #                                              (sentence2)]

    jsons = None
    with open(res_review) as files:
        for file in files:
            jsons = json.loads(file)
    intervals = getIntervals(len(jsons))
    for index, item in enumerate(intervals):
        if index == intervals[-2]:
            print("Now processing ", index, '/', len(intervals))
            reviews = parsePredictData(jsons, item, intervals[index+1])
            indexer = Indexer(reviews)
            vocab_index, index_vocab = indexer.indexVocab(vocab_index_file,index_vocab_file)
            print("  Number of vocabulary: ", len(vocab_index))
            reviews_X=[]
            for review in reviews:
                single_review_X = indexer.indexSentence(review, vocab_index, 85)
                reviews_X.append(single_review_X)
            np.save(path + "vocab.index.npy", vocab_index)
            np.save(path + "index.vocab.npy", index_vocab)
            np.savez(path + "predict_yelp/yelp_" + str(index), data=reviews_X)
    vocab_index = np.load(vocab_index_file).item()
    sorted_embeddings = data_prep.getSortedEmbeddings(vocab_index, embedding_file, 300)
    np.save(path + "emb.npy", sorted_embeddings)

def main():
    embedding_file = "../data/official_data/newglove.txt"
    vocab_index_file = "../data/prep_data/vocab.index.npy"
    index_vocab_file = "../data/prep_data/index.vocab.npy"
    path = "../data/prep_data/"
    yelp_labeled_file = "../data/prep_data_yelp/res_review_selection.csv"
    res_review = "../data/prep_data_yelp/res_review.json"

    # processYelpTrain(yelp_labeled_file,embedding_file, vocab_index_file,index_vocab_file,path)
    processYelpPredict(res_review,embedding_file,vocab_index_file,index_vocab_file,path)

if __name__ == '__main__':
    main()
