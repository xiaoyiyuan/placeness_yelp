import tensorflow as tf

class BaseModel():
    def emb_lookup(self, batch_data, emb, scope_name):
        # batch_data.shape = [batch_size, 85]
        # emb.shape=[volcabulary_size, embedding_dimension(300)]
        # lookup.shape=[batch_size, 85, 300]
        with tf.variable_scope(scope_name, reuse=tf.AUTO_REUSE) as scope:
            batch_data = tf.cast(batch_data, tf.int32)
            lookup = tf.nn.embedding_lookup(params=emb,
                                            ids=batch_data,
                                            name=scope_name)
            return lookup

    def convRelu(self, input, nfilters, fsize, stride, padding, scope_name):
        with tf.variable_scope(scope_name, reuse=tf.AUTO_REUSE):
            in_channels = input.shape[-1]
            filters = tf.get_variable(name="filters",
                                      shape=[fsize, in_channels, nfilters],
                                      initializer=tf.contrib.layers.xavier_initializer())
            biases = tf.get_variable(name="biases",
                                     shape=[nfilters],
                                     initializer=tf.contrib.layers.xavier_initializer())
            conv = tf.nn.conv1d(input, filters, stride, padding=padding)
            conv_relu = tf.nn.relu(conv + biases)
            return conv_relu

    def fully_connected(self, input, out_dim, scope_name):
        with tf.variable_scope(scope_name, reuse=tf.AUTO_REUSE):
            # input, either from maxpool layer or conv layer, is already flattened
            # ~ shape=[batches_size, #features]
            in_dim = input.shape[-1]
            w = tf.get_variable("w",
                                shape=[in_dim, out_dim],
                                initializer=tf.contrib.layers.xavier_initializer())
            b = tf.get_variable("b",
                                shape=[out_dim],
                                initializer=tf.contrib.layers.xavier_initializer())
            logits = tf.matmul(input, w) + b
            return logits

    def maxpool(self, input, fsize, stride, scope_name):
        with tf.variable_scope(scope_name, reuse=tf.AUTO_REUSE) as scope:
            maxpool = tf.nn.pool(input,
                                 [fsize],
                                 'MAX',
                                 'SAME',
                                 strides=[stride],
                                 name=scope.name)
            return maxpool





