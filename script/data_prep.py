import xml.etree.ElementTree as ET
import numpy as np
import re
import os


'''
    This script prepares data to be fed in to tensorflow.nn.embedding_lookup():
        1. embeddings
        2. X (represented in embedding indices)
        , to get embeddings for each token of each sentence, for later training purpose
    The following script reads raw data of xml format (from SemEval 2016) and:
        1. index volcabulary 
        2. turn sentences of words into sentences of indices
        3. select vocabulary embeddings and sort them based on vocabulary indices
        4. write all of these results to files under "../data/prep_data/"
    Warning:
        I have modified the data a bit so that the sentence matches target words 
        (for instance chef's into chef)
'''

class XmlParser():
    '''
        The Xml parser parses xml data to two lists (sentences, tags):
            sentences: each row is a 1-d array with real words
                [ (word, word, word, ...)
                  (word, word, word, ...)
                  ...
                  (                     ) ]
            iob:
                [ (0,0,0,1,0,0,...)
                  (0,0,1,2,0,0,...)
                  ....
                  (               ) ]
        XML structure from 2014 competition is different from 2016 competition.
    '''
    def parse2014(self, input):
        root = ET.parse(input).getroot()
        sentences_ = []
        iobs=[]
        for sentence in root:
            text = sentence[0].text
            words = re.findall(r"[\w']+|[.,!?;]", text)
            target_words = []  # list of lists (one sentence has multiple opinions)
            iob = [0] * len(words)
            if len(sentence) > 2:
                opinions = sentence[1]
                for opinion in opinions:
                    target = opinion.attrib.get('term')
                    target_words.append(re.findall(r"[\w']+|[.,!?;]", target))
                for lst in target_words:
                    index1 = words.index(lst[0])
                    iob[index1] = 1
                    if len(lst) > 1:
                        for index2 in range(index1 + 1, index1 + len(lst)):
                            iob[index2] = 2
            sentences_.append(np.array(words))
            iobs.append(np.array(iob))
        return np.array(sentences_), np.array(iobs)

    def parse2016(self, input):
        root = ET.parse(input).getroot()
        sentences_ = []
        iobs=[]
        for review in root:
            sentences = review[0]
            for sentence in sentences:
                text = sentence[0].text
                words = re.findall(r"[\w']+|[.,!?;]", text)
                target_words = []  # list of lists (one sentence has multiple opinions)
                iob = [0] * len(words)
                # if it has a explicit targets sentence[0] is text, sentence[1] is opinions
                # (some sentences only have text, no opinion (targets), keep their iob all 0s.)
                if len(sentence) > 1:
                    opinions = sentence[1]
                    for opinion in opinions:
                        target = opinion.attrib.get('target')
                        target_words.append(re.findall(r"[\w']+|[.,!?;]", target))
                    # fill out iob tagging (0:O, 1:B; 2:I)
                    if ['NULL'] in target_words:
                        target_words = [x for x in target_words if x != ['NULL']]
                    for lst in target_words:
                        index1 = words.index(lst[0])
                        iob[index1] = 1
                        if len(lst) > 1:
                            for index2 in range(index1 + 1, index1 + len(lst)):
                                iob[index2] = 2
                sentences_.append(np.array(words))
                iobs.append(np.array(iob))
        return sentences_, iobs


class Indexer():
    '''
        This class has 2 main methods:
            indexVocab: create a dictionary where {vocab: index}
            indexSentence: create a 2d array that each sentence is represented by vocab indices
                           (pad sentences to sentence_length)
        output:
            X: 2D array (each row is a sentences represented by word index)
                ( (idx, idx, idx, ...)
                  (idx, idx, idx, ...)
                  ...
                  (                  ) )
            iob: 2D array
                ( (0,0,0,1,0,0,...)
                  (0,0,1,2,0,0,...)
                  ....
                  (               ) )
            emb: 2D array (select all the word embeddings needed and sort them based on word idx
                ( (----vector-----)
                  (----vector-----)
                  ...
                  (               ) )
    '''
    def __init__(self, all_data):
        '''
        :param all_data: a list of lists of arrays
                        [train_sentences, test_sentences]
                        train/test_sentences is a list with sentence arrays
        '''
        self.all_data = all_data

    def indexVocab(self, vocab_index_file, index_vocab_file):
        exists = os.path.isfile(vocab_index_file)
        if not exists:
            vocab = []
            for data in self.all_data:
                for sentence in data:
                    if sentence.ndim != 0:
                        for word in sentence:
                          if word not in vocab:
                                vocab.append(word)
            # the index should starts from 1, because the sentence will be padded with 0
            vocab_index = {word: index+1 for index, word in enumerate(vocab)}
            index_vocab = {index+1: word for index, word in enumerate(vocab)}
        else:
            vocab_index = np.load(vocab_index_file).item()
            index_vocab = np.load(index_vocab_file).item()
            new_vocab = []
            for data in self.all_data:
                for sentence in data:
                    for word in sentence:
                        if word not in vocab_index and word not in new_vocab:
                            new_vocab.append(word)
            start = len(vocab_index)
            new_vocab_index = {word:start+index+1 for index, word in enumerate(new_vocab)}
            new_index_vocab = {start+index+1:word for index, word in enumerate(new_vocab)}
            vocab_index.update(new_vocab_index)
            index_vocab.update(new_index_vocab)
        return vocab_index, index_vocab

    def indexSentence(self, data, vocab_index, sentence_length):
        '''
        :param data: a list of sentence arrays represented in words
        :return data_indices: 2D array (an array of padded sentence arrays represented in indices)
        '''
        # ss: sentences in indices
        ss = np.zeros((len(data), sentence_length))
        for i, sentence in enumerate(data):
            s = np.zeros(ss.shape[1]) # stretch to sentence_length
            for ii, word in np.ndenumerate(sentence):
                word_index = vocab_index[word]
                s[ii] = word_index
            ss[i] = s
        return ss

def getSortedEmbeddings(vocab_index, embedding_file, embedding_dimension):
    '''
        selected and sorted embeddings based on vocab_indices: 2D array
    '''
    found = 0
    # the first row of sorted will remain 0 because vocab_index's index is from 1
    # because the sentence will be padded with 0 and the 0 should have embedding [0,0,0,....,0,0]
    sorted = np.zeros((len(vocab_index)+1, embedding_dimension))
    with open(embedding_file) as f:
        for l in f:
            vec = l.rstrip().split(' ')
            if len(vec) == 2: # skip first line
                continue
            if vec[0] in vocab_index:
                index = vocab_index[vec[0]]
                sorted[index] = vec[1:]
                found += 1
            if found == len(vocab_index):
                break
    return sorted

def adjust_iobs_length(iobs, sentence_length):
    '''
    :param iobs: a list of 1-d arrays
    :return: 2D array, padded iobs (length = sentence_length)
    '''
    ll = np.zeros((len(iobs), sentence_length))
    for i, label in np.ndenumerate(iobs):
        l = np.zeros(ll.shape[1])
        l[:len(label)] = label
        ll[i] = l
    return ll

def process(trainfile, testfile, embedding_file, vocab_index_file, index_vocab_file, path, year):
    print("...Now parsing xml...")
    train_sentences, train_iobs, test_sentences, test_iobs = None, None, None, None
    if year == "2016":
        train_sentences, train_iobs = XmlParser().parse2016(trainfile)
        test_sentences, test_iobs = XmlParser().parse2016(testfile)
    if year == '2014':
        train_sentences, train_iobs = XmlParser().parse2014(trainfile)
        test_sentences, test_iobs = XmlParser().parse2014(testfile)

    print("...Now writing parsed xml to files...")
    # when loaded, these data will be ndarray with !!!dtype=object
    np.savez(path + "train_sentences_iobs" + year + ".npz", X=train_sentences, y=train_iobs)
    np.savez(path + "test_sentences_iobs" + year + ".npz", X=test_sentences, y=test_iobs)

    print("...Now index vocabulary, sentences, and embeddings...")
    indexer = Indexer([train_sentences, test_sentences])
    vocab_index, index_vocab = indexer.indexVocab(vocab_index_file, index_vocab_file)
    sorted_embeddings = getSortedEmbeddings(vocab_index, embedding_file, 300)
    sentence_length = 85
    train_X = indexer.indexSentence(train_sentences, vocab_index, sentence_length)
    train_y = adjust_iobs_length(train_iobs, sentence_length)
    test_X = indexer.indexSentence(test_sentences, vocab_index, sentence_length)
    test_y = adjust_iobs_length(test_iobs, sentence_length)

    print("...Now writing indexed data to files...")
    np.save(path + "vocab.index", vocab_index)
    np.save(path + "index.vocab", index_vocab)
    np.savez(path + "train" + year + ".npz", X=train_X, y=train_y)
    np.savez(path +"test" + year + ".npz", X=test_X, y=test_y)
    np.save(path + "emb.npy", sorted_embeddings)

def main():
    # if index.vocab and vocab.index already exists (when adding more triaining data):
    # the index.vocab, vocab.index will expand (new vocabularies appended)
    # new emb, train, test, train_sentences_iobs, test_sentences_iobs files will be created.
    trainfile16 = "../data/official_data/ABSA16_Restaurants_Train_SB1_v2.xml"
    testfile16 = "../data/official_data/EN_REST_SB1_TEST.xml.gold"
    trainfile14 = "../data/official_data/ABSA14_Restaurants_Train.xml"
    testfile14 = "../data/official_data/ABSA14_Restaurants_Test_Data_phaseB.xml"
    embedding_file = "../data/official_data/newglove.txt"
    vocab_index_file = "../data/prep_data/vocab.index.npy"
    index_vocab_file = "../data/prep_data/index.vocab.npy"
    path = "../data/prep_data/"
    process(trainfile16, testfile16, embedding_file,
            vocab_index_file, index_vocab_file, path, year='2016')
    process(trainfile14, testfile14, embedding_file,
            vocab_index_file, index_vocab_file, path, year='2014')



if __name__ == '__main__':
    main()


