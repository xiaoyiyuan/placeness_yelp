import tensorflow as tf
import numpy as np

'''
    All the data reading and preperation for the model, including:
        batching training and testing data and create iterator for the batched data
        create embedding tf constant 
        other data processing steps packing padded x and y for softmax
'''

def getSemEvalData():
    '''
        Train and Test using Sem-Eval 2014+2016
        To show that my implementation gets to the similar result
        But it performs really bad on predicting Yelp data (~50% f1)
    '''
    train_path1 = "../data/prep_data/train2016.npz"
    train_path2 = "../data/prep_data/train2014.npz"
    train_data1, train_data2 = np.load(train_path1), np.load(train_path2)
    train_x1, train_y1 = train_data1['X'], train_data1['y']
    train_x2, train_y2 = train_data2['X'], train_data2['y']
    train_x = np.concatenate([train_x1,train_x2], axis=0)
    train_y = np.concatenate([train_y1,train_y2], axis=0)
    train_x_lens = getXLen(train_x)
    train = (train_x_lens, train_x, train_y)
    train = tf.data.Dataset.from_tensor_slices(train)
    test_path1 = "../data/prep_data/test2016.npz"
    test_path2 = "../data/prep_data/test2014.npz"
    test_data1, test_data2 = np.load(test_path1), np.load(test_path2)
    test_x1, test_y1 = test_data1['X'], test_data1['y']
    test_x2, test_y2 = test_data2['X'], test_data2['y']
    test_x = np.concatenate([test_x1, test_x2], axis=0)
    test_y = np.concatenate([test_y1, test_y2], axis=0)
    test_x_lens = getXLen(test_x)
    test = (test_x_lens, test_x, test_y)
    test = tf.data.Dataset.from_tensor_slices(test)
    return train, test

def getMixedData():
    '''
        Train model with the mixture of Sem-Eval 2014 and Yelp train
        Test model with Yelp test
    '''
    train_path1 = "../data/prep_data/train2014.npz"
    train_path2 = "../data/prep_data/train_yelp.npz"
    train_data1, train_data2 = np.load(train_path1), np.load(train_path2)
    train_x1, train_y1 = train_data1['X'], train_data1['y']
    train_x2, train_y2 = train_data2['X'], train_data2['y']
    train_x = np.concatenate([train_x1,train_x2], axis=0)
    train_y = np.concatenate([train_y1,train_y2], axis=0)
    train_x_lens = getXLen(train_x)
    train = (train_x_lens, train_x, train_y)
    train = tf.data.Dataset.from_tensor_slices(train)
    test_path = "../data/prep_data/test_yelp.npz"
    test_data = np.load(test_path)
    test_x, test_y = test_data['X'], test_data['y']
    test_x_lens = getXLen(test_x)
    test = (test_x_lens, test_x, test_y)
    test = tf.data.Dataset.from_tensor_slices(test)
    return train, test

def getYelpData():
    '''
        Train and test model with Yelp data only
    '''
    train_path = "../data/prep_data/train_yelp.npz"
    train_data = np.load(train_path)
    train_x, train_y = train_data['X'], train_data['y']
    train_x_lens = getXLen(train_x)
    train = (train_x_lens, train_x, train_y)
    train = tf.data.Dataset.from_tensor_slices(train)
    test_path = "../data/prep_data/test_yelp.npz"
    test_data = np.load(test_path)
    test_x, test_y = test_data['X'], test_data['y']
    test_x_lens = getXLen(test_x)
    test = (test_x_lens, test_x, test_y)
    test = tf.data.Dataset.from_tensor_slices(test)
    return train, test, len(test_x)

def getPredictData():
    path = "../data/prep_data/predict.npy"
    x = np.load(path)
    x_lens = getXLen(x)
    data = (x, x_lens)
    size = len(x)
    predict_data = tf.data.Dataset.from_tensor_slices(data)
    return predict_data, size

def getEmbeddings():
    '''
        get embeddings prepared for modeling training
    '''
    emb = np.load("../data/prep_data/emb.npy").astype(np.float32)
    embeddings = tf.constant(emb, name='embeddings')
    return embeddings


def getXLen(x):
    length = np.zeros(x.shape[0])
    for i, s in enumerate(x):
        s = np.trim_zeros(s, 'b') # 'b': trim the end
        length[i] = s.shape[0]
    return length

def pack_y(lens, y):
    '''
        For softmax, concatenate all labels in a batch into a long vector.
        Because the labels are padded with 0, get rid of the paddings.
        Argument "lens" has information of the real length of each sentence.
    '''
    lens = tf.reshape(lens, [-1, 1])
    mask = tf.map_fn(
        lambda x: tf.concat([tf.ones(tf.cast(x,tf.int32), dtype=tf.float64),
                             tf.zeros(tf.cast(85-x,tf.int32), dtype=tf.float64)],axis=0),
        lens,
        tf.float64,
        infer_shape=False)
    mask.set_shape([None, 85])
    # the result of pack_y is a vector, so original shape does not retain with boolean.mask
    packed_y = tf.boolean_mask(y, mask)
    packed_y = tf.cast(packed_y, tf.int32)
    packed_y = tf.one_hot(packed_y, depth=3)
    return packed_y

def pack_logits(lens, logits):
    '''
        Same as pack_y, except y is (batch, 85), x is (batch*85, 3)
        So the mask needed is (batch*85, 3)
    '''
    lens = tf.reshape(lens, [-1, 1])
    mask = tf.map_fn(
        lambda x: tf.concat([tf.ones(tf.cast(x, tf.int32), dtype=tf.float64),
                             tf.zeros(tf.cast(85 - x, tf.int32), dtype=tf.float64)], axis=0),
        lens,
        tf.float64,
        infer_shape=False)
    mask.set_shape([None, 85])
    mask = tf.reshape(mask, [-1, 1])
    masks = tf.reshape(tf.stack([mask, mask, mask], axis=1), [-1, 3])
    packed_logits = tf.boolean_mask(logits, masks)
    packed_logits = tf.reshape(packed_logits, [-1, 3])
    return packed_logits

